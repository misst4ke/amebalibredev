#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Used to create new users and databases (same name) on postgres container."""

import sys
import subprocess
from argparse import ArgumentParser


def execute(command, postgres):
    """Execute a postgres command."""
    error = subprocess.run([
        'docker', 'exec', '-it', postgres,
        'psql', '-U', 'postgres', '-c', command
    ]).returncode

    if(error):
        print('ABORT!')
        exit()

    return error


def _get_params():
    parser = ArgumentParser()
    parser.add_argument(
        "-u", "--user",
        dest="user",
        help="Username and Database name",
        metavar="USERNAME",
        required=True)
    parser.add_argument(
        "-c", "--container",
        dest="container",
        help="Container name of database",
        metavar="CONTAINER_NAME",
        required=True)
    return parser.parse_args()


def main(target, password, postgres):
    commands = [
        "create database {0};".format(target),
        "create user {0} with encrypted password '{1}';".format(target, password),
        "grant all privileges on database {0} to {0};".format(target)
    ]
    for command in commands:
        execute(command, postgres)


if(__name__ == '__main__'):
    if sys.version_info[0] < 3:
        raise Exception("Must be using Python 3+")

    print('Script {0} start!'.format(__file__))

    params = _get_params()
    password = input('new password for user {0}: '.format(params.user))

    if(not password or password.strip() == ''):
        print('You need specify a password!')

    main(params.user, password, params.container)
    print('Script {0} end!'.format(__file__))
